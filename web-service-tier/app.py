from flask import Flask, Response
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
import requests
import json 
import time

DAO_LINK = "http://127.0.0.1:5001/"

app = Flask(__name__)

CORS(app, supports_credentials =True)

@app.route("/stream")
def stream():
    link = DAO_LINK+"stream"
    r = requests.get(link, stream=True)

    def event_stream():
        for my_json in r.iter_lines():
            
            if my_json:
                yield 'data: {}\n\n'.format(my_json.decode("ascii"))
    return Response(event_stream(), status=200, mimetype="text/event-stream")  

@app.route("/test")
def hello():
    return "Hello World!"

@app.route("/time")
def currenttime_for_web():
    def eventStream():
        while True:
            time.sleep(1)
            yield 'data: {}\n\n'.format(currenttime())
    return Response(eventStream(), status = 200, mimetype="text/event-stream")

def currenttime():
    time.sleep(0)
    my_time = time.ctime(time.time())
    return my_time

@app.route("/login", methods=["POST"])
def login():     
    parser = reqparse.RequestParser()
    parser.add_argument('username', help = 'This field cannot be blank', required = True)
    parser.add_argument('password', help = 'This field cannot be blank', required = True)
    data = parser.parse_args()
    print(data)
    ''' connect to dao and request'''
    link = DAO_LINK + 'login'
    r = requests.post(link, data)
    for my_json in r.iter_lines():
        if my_json:
            response_line = json.loads(my_json)
            return Response(json.dumps(dict(state=response_line['state'])),  status = 200, mimetype="application/json")
    return Response(json.dumps(dict(state='Server is not responding')),  status = 500, mimetype="application/json")

def bootapp():
    app.run(port=6000, debug=True, threaded=True, host=("0.0.0.0"))

if __name__ == "__main__":
    bootapp()
