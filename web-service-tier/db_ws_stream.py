import time
from flask import Flask, Response
from flask_cors import CORS
#from connect_to_db import DealTableLoader

app = Flask(__name__)
CORS(app)


@app.route('/')
@app.route('/index')
def index():
    return "Hello world"


@app.route('/streamTest')
def stream():
    def eventStream():
        while True:
            # wait for source data to be available, then push it
            yield get_message() + "\n"

    return Response(eventStream(), mimetype="text/event-stream")


def get_message():
    """this could be any function that blocks until data is ready"""
    time.sleep(1.0)
    s = time.ctime(time.time())
    return s


if __name__ == '__main__':
    app.run(port=8080, threaded=True, host=('0.0.0.0'))