export default {
  items: [
    //{
      //name: 'login',
      //url: '/login',
      //icon: 'icon-speedometer',
      // badge: {
      //   variant: 'info',
      //   //text: 'NEW',
      // },
    //},
    // {
    //   title: false,
    //   name: 'Theme',
    //   wrapper: {            // optional wrapper object
    //     element: '',        // required valid HTML5 element tag
    //     attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: ''             // optional class names space delimited list for title item ex: "text-center"
    // },
    {
      name: 'Live Stream',
      url: '/theme/livestream',
      icon: 'fa-spin fa fa-circle-o-notch',
    },
    {
      name: 'Averages',
      url: '/theme/Averages',
      icon: 'fa fa-calculator',
    },
    // {
    //   title: false,
    //   name: 'Components',
    //   wrapper: {
    //     element: '',
    //     attributes: {},
    //   },
    //},
    {
      name: 'Historie',
      url: '/base',
      icon: 'fa fa-history fa-lg mt-2',
      children: [
        {
          name: 'Charts',
          url: '/base/charts',
          icon: 'fa fa-line-chart',
        },
        {
          name: 'Tables',
          url: '/base/tables',
          icon: 'fa fa-table fa-lg',
        },          
               
        ],
    },
    {
      name: 'Reports',
      url: '/reports',
      icon: 'fa fa-file-text',
      children: [
        {
          name: 'Ending Positions',
          url: '/buttons/ending_positions',
          icon: 'cui-sort-descending',
        },
        {
          name: 'Realised Profit',
          url: '/buttons/realised-profit',
          icon: 'fa fa-money',
        },
        {
          name: ' Effective Profit',
          url: '/buttons/ effective profit-loss ',
          icon: 'fa fa-briefcase ',
        },        
      ],
    },      
     
      
  ],
};
